[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# GNOME DAV Proxy
Transparently add CalDAV and CardDAV support to GNOME client applications, through GNOME Online Accounts, that does not support these protocols.

## Motivation and capabilities
Most calendar, contacts, and tasks services support the CalDAV and CardDAV protocols, but GNOME Online Accounts does not.
This script creates a simple local proxy that intercepts the CalDAV and CardDAV  and transparently replaces them with Nextcloud connections.
GNOME Online Accounts can use the Nextcloud option, with no need to make it aware of CalDAV and CardDAV's existence.

## Getting started
After cloning or [downloading](https://gitlab.com/julianfairfax/gnome-dav-proxy) this repository, start by creating a `gnome-dav-proxy.config` file to add configuration details for the CalDAV and CardDAV connections that you want to use with the proxy.
For example, for EteSync, you would create a file that looks like this:
```
[gnome-dav-proxy]
cal = http://localhost:37358
card = http://localhost:37358
```
For other services, replace the URLs.

Next, from a terminal, start the proxy: `python gnome-dav-support.py`.

Finally, open GNOME Online Accounts and configure a Nextcloud account with your CalDAV and CardDAV account's login details, and use `http://localhost:8224` as the server.

## Required arguments and configuration
When starting the proxy there is one required argument that must be set to configure its behaviour.

- `--config_file` to specify the location of the configuration file that the proxy should load.
If this argument is not provided, the proxy will not run.

### Starting the proxy automatically
In order for the proxy to authenticate background requests from GNOME Online Accounts it needs to be kept running constantly.
The easiest way to do this is to start the script automatically.

It is possible to run the proxy as a service (e.g., via `systemctl`):
```
[Unit]
Description=GNOME CalDAV and CardDAV Proxy

[Service]
Type=simple
ExecStart=python3 /path/to/gnome-dav-proxy.py --config_file /path/to/config.config

[Install]
WantedBy=default.target
```

### Problems
Please feel free to [open an issue](https://gitlab.com/julianfairfax/gnome-dav-proxy/-/issues) reporting any bugs you find, or [submit a pull request](https://gitlab.com/julianfairfax/gnome-dav-proxy/-/merge_requests) to help improve this tool.

## Related projects and alternatives
This tool primarily consists of [this script](https://gist.github.com/apollo13/f4fc8f33a2700dffb9e11c1b056c53ba?permalink_comment_id=4286019#gistcomment-4286019) by [@phs](https://gist.github.com/phs).

## License
[MIT](LICENSE.txt)
