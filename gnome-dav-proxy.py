#!/usr/bin/env python3

import os
from http.server import ThreadingHTTPServer, BaseHTTPRequestHandler

import argparse
import pathlib
parser = argparse.ArgumentParser()
parser.add_argument('--config_file', type=pathlib.Path)
args = parser.parse_args()

import configparser
config = configparser.ConfigParser()
config.read(args.config_file)
caldav_url = config.get('gnome-dav-proxy', 'cal')
carddav_url = config.get('gnome-dav-proxy', 'card')

HOST = os.getenv('HOST', 'localhost')
PORT = int(os.getenv('PORT', '8224'))
CALDAV = os.getenv('CALDAV', caldav_url)
CARDDAV = os.getenv('CARDDAV', carddav_url)

REDIRECTS = {
        '/.well-known/caldav': CALDAV,
        '/.well-known/carddav': CARDDAV,
        '/remote.php/caldav/': CALDAV,
        '/remote.php/carddav/': CARDDAV,
        }

class Redirect(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/remote.php/webdav/':
            self.send_response(200)
            self.end_headers()

    def do_PROPFIND(self):
        location = REDIRECTS.get(self.path)
        if location:
            self.send_response(302)
            self.send_header('Location', location)
            self.end_headers()

ThreadingHTTPServer((HOST, PORT), Redirect).serve_forever()
